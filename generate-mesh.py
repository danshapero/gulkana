from collections import OrderedDict
import subprocess
import numpy as np
import geojson
import fiona
import pyproj
import icepack

with fiona.open("01_rgi60_Alaska.shp", "r") as source:
    candidates = [
        item for item in source
        if (name := item["properties"]["Name"]) is not None
        and name == "Gulkana Glacier"
    ]
    assert len(candidates) == 1
    outline = candidates[0]


lat_lon = pyproj.CRS(4326)
utm_zone8 = pyproj.CRS(32608)
transformer = pyproj.Transformer.from_crs(lat_lon, utm_zone8)

for index, segment in enumerate(outline["geometry"]["coordinates"]):
    coords = np.array(segment)
    lon, lat = coords[:, 0], coords[:, 1]
    x, y = transformer.transform(lat, lon)
    outline["geometry"]["coordinates"][index] = list(zip(x[:-1], y[:-1]))

outline["geometry"]["type"] = "MultiLineString"
outline["properties"] = OrderedDict()

meta = {
    "driver": "GeoJSON",
    "schema": {"properties": OrderedDict(), "geometry": "MultiLineString"},
    "crs": {"init": "epsg:32608"},
}
with fiona.open("gulkana.geojson", "w", **meta) as destination:
    destination.write(outline)

with open("gulkana.geojson", "r") as outline_file:
    outline = geojson.load(outline_file)

outline = icepack.meshing.normalize(outline)
geometry = icepack.meshing.collection_to_geo(outline)
with open("gulkana.geo", "w") as geometry_file:
    geometry_file.write(geometry.get_code())

command = "gmsh -2 -format msh2 -v 0 -o gulkana.msh gulkana.geo"
subprocess.run(command.split(" "))
